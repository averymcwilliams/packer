'use strict';

const fs = require('fs');
const path = require('path');

class WZWriter {
  constructor(initialSize=1024) {
    this.offset = 0;
    this.buffer = Buffer.alloc(initialSize);
    this.stringToOffset = new Map();
    this.canvasToOffset = new Map();
    this.audioToOffset = new Map();
  }
  getNextEmptyBuffer(length) {
    const nextPowerOf2 = 2 ** Math.ceil(Math.log2(length));
    return Buffer.alloc(length*2);
  }
  expandIfNeeded(numBytes) {
    const numBytesNeeded = this.offset + numBytes;
    if (numBytesNeeded > this.buffer.length) {
      const newBuffer = this.getNextEmptyBuffer(numBytesNeeded);
      this.buffer.copy(newBuffer);
      this.buffer = newBuffer;
    }
  }
  getBuffer() {
    return this.buffer.slice(0, this.offset);
  }
  writeInt8(i8) {
    const worstIncrease = 1;
    this.expandIfNeeded(worstIncrease);
    this.buffer.writeInt8(i8, this.offset);
    this.offset += 1;
  }
  writeUInt8(ui8) {
    const worstIncrease = 1;
    this.expandIfNeeded(worstIncrease);
    this.buffer.writeUInt8(ui8, this.offset);
    this.offset += 1;
  }
  writeCompressedInt16(i16) {
    const worstIncrease = 3;
    this.expandIfNeeded(worstIncrease);
    if (i16 >= -0x7f && i16 <= 0x7f) {
      this.writeInt8(i16);
    } else {
      this.writeInt8(-0x80);
      this.buffer.writeInt16BE(i16, this.offset);
      this.offset += 2;
    }
  }
  writeCompressedUInt16(ui16) {
    const worstIncrease = 3;
    this.expandIfNeeded(worstIncrease);
    if (ui16 <= 254) {
      this.writeUInt8(ui16);
    } else {
      this.writeUInt8(0xff);
      this.buffer.writeUInt16BE(ui16, this.offset);
      this.offset += 2;
    }
  }
  writeCompressedInt32(i32) {
    const worstIncrease = 5;
    this.expandIfNeeded(worstIncrease);
    if (i32 >= -0x7e && i32 <= 0x7f) {
      this.writeInt8(i32);
    } else if (i32 >= -0x8000 && i32 <= 0x7fff) {
      this.writeInt8(-0x7f);
      this.buffer.writeInt16BE(i32, this.offset);
      this.offset += 2;
    } else {
      this.writeInt8(-0x80);
      this.buffer.writeInt32BE(i32, this.offset);
      this.offset += 4;
    }
  }
  writeCompressedUInt32(ui32) {
    const worstIncrease = 5;
    this.expandIfNeeded(worstIncrease);
    if (ui32 >= 0x00 && ui32 <= 0xfd) {
      this.writeUInt8(ui32);
    } else if (ui32 >=0 && ui32 <= 0xffff) {
      this.writeUInt8(0xfe);
      this.buffer.writeUInt16BE(ui32, this.offset);
      this.offset += 2;
    } else {
      this.writeUInt8(0xff);
      this.buffer.writeUInt32BE(ui32, this.offset);
      this.offset += 4;
    }
  }
  writeStringOrDuplicate(s) {
    const encoding = 'UTF-8';
    const bytes = Buffer.from(s, encoding);
    const worstIncrease = bytes.length + 1 + 3;
    this.expandIfNeeded(worstIncrease);
    if (bytes.length <= 3) {
      this.writeInt8(0x00);
      this.writeInt8(bytes.length);
      bytes.copy(this.buffer, this.offset);
      this.offset += bytes.length;
    } else if (this.stringToOffset.has(s)) {
      this.writeInt8(0x01);
      this.buffer.writeUInt32BE(this.stringToOffset.get(s), this.offset);
      this.offset += 4;
    } else {
      this.writeInt8(0x00);
      this.stringToOffset.set(s, this.offset);
      this.writeCompressedUInt16(bytes.length);
      bytes.copy(this.buffer, this.offset);
      this.offset += bytes.length;
    }
  }
  writeCanvasOrDuplicate(c) {
    const encoding = 'base64';
    const withoutPrefix = c.b.slice(24);
    const bytes = Buffer.from(withoutPrefix, encoding);
    const worstIncrease = bytes.length + 1 + 3 + 3 + 5;
    this.expandIfNeeded(worstIncrease);
    if (this.canvasToOffset.has(withoutPrefix)) {
      this.writeInt8(0x01);
      this.buffer.writeUInt32BE(
        this.canvasToOffset.get(withoutPrefix),
        this.offset
      );
      this.offset += 4;
    } else {
      this.writeInt8(0x00);
      this.canvasToOffset.set(withoutPrefix, this.offset);
      this.writeCompressedUInt16(c.w);
      this.writeCompressedUInt16(c.h);
      this.writeCompressedUInt32(bytes.length);
      bytes.copy(this.buffer, this.offset);
      this.offset += bytes.length;
    }
  }
  writeAudioOrDuplicate(s) {
    const encoding = 'base64';
    const bytes = Buffer.from(s, encoding);
    const worstIncrease = bytes.length + 1;
    this.expandIfNeeded(worstIncrease);
    if (this.audioToOffset.has(s)) {
      this.writeInt8(0x01);
      this.buffer.writeUInt32BE(this.audioToOffset.get(s), this.offset);
      this.offset += 4;
    } else {
      this.writeInt8(0x00);
      this.audioToOffset.set(s, this.offset);
      this.writeCompressedUInt32(bytes.length);
      bytes.copy(this.buffer, this.offset);
      this.offset += bytes.length;
    }
  }
  writeFloat32(f32) {
    const worstIncrease = 4;
    this.expandIfNeeded(worstIncrease);
    this.buffer.writeFloatBE(f32, this.offset);
    this.offset += 4;
  }
  writeFloat64(f64) {
    const worstIncrease = 8;
    this.expandIfNeeded(worstIncrease);
    this.buffer.writeDoubleBE(f64, this.offset);
    this.offset += 8;
  }
}

function serializeObj(obj, writer) {
  if (obj .t === 0 || obj.t === 1) {
    // delete .wz and .img when writing to save space
    const arr = obj.n.split('.');
    const n = arr.length === 1 ? arr[0] : arr.slice(0, -1).join('.');
    writer.writeStringOrDuplicate(n);
  } else {
    writer.writeStringOrDuplicate(obj.n);
  }
  writer.writeInt8(obj.t);

  if (obj.hasOwnProperty('c') && obj.c.length === 0) {
    throw new Error('Empty child array');
  }

  if (obj.t === 0) {
    const numChildren = !obj.c ? 0 : obj.c.length;
    writer.writeCompressedUInt16(numChildren);
    if (numChildren !== 0) {
      obj.c.forEach(child => serializeObj(child, writer));
    }
  } else if (obj.t === 1) {
    const numChildren = !obj.c ? 0 : obj.c.length;
    writer.writeCompressedUInt16(numChildren);
    if (numChildren !== 0) {
      obj.c.forEach(child => serializeObj(child, writer));
    }
  } else if (obj.t === 2) {
    const numChildren = !obj.c ? 0 : obj.c.length;
    writer.writeCompressedUInt16(numChildren);
    if (numChildren !== 0) {
      obj.c.forEach(child => serializeObj(child, writer));
    }
  } else if (obj.t === 3) {
    writer.writeCanvasOrDuplicate(obj);
    const numChildren = !obj.c ? 0 : obj.c.length;
    writer.writeCompressedUInt16(numChildren);
    if (numChildren !== 0) {
      obj.c.forEach(child => serializeObj(child, writer));
    }
  } else if (obj.t === 4) {
    const numChildren = !obj.c ? 0 : obj.c.length;
    writer.writeCompressedUInt16(numChildren);
    if (numChildren !== 0) {
      obj.c.forEach(child => serializeObj(child, writer));
    }
  } else if (obj.t === 5) {
    writer.writeAudioOrDuplicate(obj.b);
  } else if (obj.t === 6) {
    writer.writeStringOrDuplicate(obj.v);
  } else if (obj.t === 7) {
    writer.writeCompressedInt32(obj.v);
  } else if (obj.t === 8) {
    writer.writeFloat64(obj.v);
  } else if (obj.t === 9) {
  } else if (obj.t === 10) {
    writer.writeStringOrDuplicate(obj.v);
  } else if (obj.t === 11) {
    writer.writeCompressedInt16(obj.v);
  } else if (obj.t === 12) {
    writer.writeCompressedInt16(obj.x);
    writer.writeCompressedInt16(obj.y);
  } else if (obj.t === 13) {
    writer.writeFloat32(obj.v);
  } else {
    throw new Error(`Unknown type ${obj.t}`);
  }
}

function getObj(p) {
  if (!fs.statSync(p).isDirectory()) {
    return JSON.parse(fs.readFileSync(p, 'utf8'));
  }
  const c = fs.readdirSync(p)
    .map(s => path.join(p, s))
    .filter(f => fs.statSync(f).isFile() || fs.readdirSync(f).length > 0)
    .map(getObj);
  const obj = {
    n: p.split('/').reverse()[0],
    t: 0,
    c,
  };
  return obj;
}

const obj = getObj('wz');
console.log('Read all files');
const writer = new WZWriter();
serializeObj(obj, writer);
console.log('Serialized object');
fs.writeFileSync('binWZ', writer.getBuffer());
console.log('Dumped serialized result');
