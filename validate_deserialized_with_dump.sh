#!/usr/bin/env bash

docker build -t packer_js packer_js
docker run --cidfile packer_js_id -v `pwd`/deserialized:/packer/deserialized -v `pwd`/dump:/packer/dump packer_js node validate.js deserialized dump

PACKER_JS_CONTAINER_ID=`cat packer_js_id`

docker rm -f $PACKER_JS_CONTAINER_ID
rm -f packer_js_id
